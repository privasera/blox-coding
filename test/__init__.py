#!/usr/bin/env python

import sys
sys.path.append('../../..')

from utils.http.tester import Tester
from utils.log import Log

ROUNDS = 5
USERS = 200

projects = []

log = Log('debug.txt')
log.write('debug', 'test')

def users_pre_post(tester, route, obj):
    log.write('debuh', 'users_pre_post');
    return route['url'], {}

def users_post_post(tester, response, obj):
    log.write('debug', 'users_post_post')
    return response 

def projects_pre_post(tester, route, obj):
    log.write('debug', 'projects_pre_post')
    return route['url'], {}

def projects_post_post(tester, response, obj):
    log.write('debug', 'projects_post_post')
    print response
    return

# I seperated REST calls to seperate urls
ROUTES = [
    # Add users
    {
        'url': '/editor/register/',
        'attributes': [('username', 'string'), ('password', 'string')],
        'create': None,
        'compare': None,
        'strict': 'No',
        'frequency': [('post', 100)],
        'pre_post': users_pre_post,
        'post_post': users_post_post
    },
    # Add Projects
    {
        'url': '/editor/addproject/',
        'attributes': [('name', 'string')],
        'create': None,
        'compare': None,
        'strict': 'No',
        'frequency': [('post', 100)],
        'pre_post': projects_pre_post,
        'post_post': projects_post_post
    },
    
]

def create_projects(tester, number):
    log.write('debug', 'create_projects - start', number=number)

    for i in range(0, number):
        log.write('debug', 'create_projects - start round', i=i)

        tester.post(ROUTES[0])

    log.write('debug', 'create_bubbles - finish') 
    return

def run_test(tester, g_log, n_rounds):
    global log
    log = g_log
    log.write('debug', 'run_test - start')

    create_projects(tester)

    tester.run_test(n_rounds)

    log.write('debug', 'run_test - finish')

if __name__ == '__main__':
    log.clear()
    tester = Tester('http://127.0.0.1:8000', test_objects=ROUTES, delay=2)
    tester.run_test(ROUNDS, log)
