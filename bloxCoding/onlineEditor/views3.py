from django.shortcuts import render, render_to_response, get_object_or_404
from django.views.generic import View
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from onlineEditor.forms import UserForm, ProjectForm, FileForm
from onlineEditor.models import UserProfile, Project, File
from onlineEditor.filemanip import *
import json

class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)

class UserRegisterView(FormView):
    template_name = 'register.html'
    form_class = UserForm
    success_url = 'index'

    def form_valid(self, form):
        

class UserLoginView(View):
    pass

