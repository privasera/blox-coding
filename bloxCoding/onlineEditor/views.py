""" @package docstring 
#  testing doxygen head
#
#  More detes.
"""

from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.views.generic import View
from django.template import RequestContext
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, BadHeaderError
from onlineEditor.forms import UserForm, ProjectForm, FileForm
from onlineEditor.models import UserProfile, Project, File
from onlineEditor.filemanip import *
from bloxCoding.secret_settings import *
import json
import os
import subprocess
import shutil
import re
import datetime

# TODO: check if forms are ever passed in as created
"""
    Testing doxygen
"""
## Testing doxygen
#
#  More details
#

# Index -returns page with Announcements
# Am going to change to make projects gather page the index 
class IndexView(View):

    def get(self, request):
        context = RequestContext(request)
        return render_to_response('onlineEditor/index.html', {}, context)

class RegisterView(View):

    def get(self, request, form=None):
        context = RequestContext(request)
        if (request.user.is_authenticated()):
            return HttpResponseRedirect('/editor/')
        if form is None:
            user_form = UserForm()
        else:                                                           
            user_form = form
        return render_to_response('onlineEditor/register.html', { 
                'user_form': user_form
                }, context)

    def post(self, request):
        context = RequestContext(request)
        user_form = UserForm(data=request.POST)
        profile = UserProfile()

        if (user_form.is_valid()):
            user = user_form.save()
            user.set_password(user.password)
                                                                                                                
            user.save()                                                                                     
            profile.user = user
            profile.save()

            #maek folder for user
            #changeMultDir('onlineEditor/codeFiles')
            goToSavedPath()
            makeUserDir(str(user.id))
            changeDir(SAVED_PATH)

            return HttpResponseRedirect('/editor/login/')
        else:
            print(user_form.errors)
            return self.get(request, user_form)

class LoginView(View):

    def get(self, request, err=''):
        context = RequestContext(request)
        if (request.user.is_authenticated()):
            return HttpResponseRedirect('/editor/')
        return render_to_response('onlineEditor/login.html', {'error': err}, context)

    def post(self, request):
        context = RequestContext(request)
        username = request.POST['username']
        password = request.POST['password']
        userProf = authenticate(username = username, password = password)
        if (userProf):
            login(request, userProf)

            # checks to see if user has folder, otherwise makes one
            # will probably only be used when testing (ex. we created
            # a user before we added folder functionality.
            # could probably remove, but redundancy is not too bad
            changeMultDir('onlineEditor/codeFiles')
            idfolder = str(userProf.id)
            try:
                os.chdir(idfolder)
                print(os.getcwd())
            except:
                makeUserDir(idfolder)

            return HttpResponseRedirect('/editor/')
        else:
            return self.get(request, "Invalid Login")

class LogoutView(View):
    # I should make logout a post
    def get(self, request):
        logout(request)
        goToSavedPath()
        return HttpResponseRedirect('/editor/')

class ProjectFormView(View):

    def get(self, request, form=None):
        context = RequestContext(request)

        # check if the form has been created yet
        if form is None:
            project_form = ProjectForm()
        else:
            project_form = form
        return render_to_response('onlineEditor/addProject.html', {
            'project_form': project_form
            }, context)


    def post(self, request):
        context = RequestContext(request)

        # check to see if request is delete request
        if ('projID' in request.POST):
            response = render_to_response('onlineEditor/redirect.html', self.delete(request), context)
            response['BLOX-dirty'] = '/editor/testgather2'
            return response 
        # handle POST request
        else:
            user = UserProfile.objects.get(user=request.user)
            project_form = ProjectForm(userID=user, data=request.POST)
            if (project_form.is_valid()):
                project = project_form.save(commit=False)
                project.userID = user
                project.save()

                #adding project folder
                try:
                    print('changing directory before adding project')
                    #changeMultDir('onlineEditor/codeFiles/')
                    goToSavedPath()
                    print(os.getcwd())
                    changeDir(str(request.user.id))
                    print( 'in user folder ' + os.getcwd())
                    addProjDir(str(project.id))

                except Exception as e:
                    print(e)
                
                createEmptyFile(project, request)                

                #response = render_to_response('onlineEditor/redirect.html', {}, context)
                #response['BLOX-dirty'] = '/editor/testgather2'
                #print 'made blox dirty'
                #return response
                return HttpResponseRedirect('/editor/projects')
            else:
                print(project_form.errors)
                return self.get(request, project_form)

    # TODO: Add error checking
    def delete(self, request):
        projID = request.POST['projID']

        #delete project folder
        try:
            goToSavedPath()
            changeDir(str(request.user.id))
            shutil.rmtree(str(projID))
        except Exception as e:
            print(e)

        # delete project folder from database
        Project.objects.get(id=projID).delete()
        return {"result": ""}

class ProjectDetailView(View):

    def get(self, request):
        try:
            # use request.user.id-1 if you have created a Django admin
            # Find a way to check for this
            projects = Project.objects.filter(userID=request.user.id)
            return render(request, 'onlineEditor/projects.html', { 'projects': projects})
        except:
            return render(request, 'onlineEditor/projects.html', )

class FileFormView(View):

    def get(self, request, form=None):
        context = RequestContext(request)

        # check to see if request is update
        if ('newName' in request.GET):
            print 'rename'
            return self.update(request)
        # check if the form has been created yet
        else:
            if form is None:
                file_form = FileForm()
            else:
                file_form = form
            return render_to_response('onlineEditor/addFile.html', {
                'file_form': file_form
                }, context)

    def post(self, request):
        context = RequestContext(request)

        # check to see if request is delete request
        if ('fileID' in request.POST):
            return self.delete(request)
        else:
            projID = request.session["projID"]
            project = Project.objects.get(id=projID)

            file_form = FileForm(projectID=project, data=request.POST)
            if (file_form.is_valid()):
                file = file_form.save(commit=False)                
                path = getSavedPath() + str(request.user.id) + '/' + str(projID)
                file.path = path
                file.projectID = project
                time = datetime.datetime.now()
                file.lastSaved = time
                file.save()

                # make file in project folder
                makeFile(file.path, file.name)

                request.session["currFileID"] = file.id
                response = HttpResponseRedirect('/editor/IDE/')
                #response.__setitem('BLOX-dirty', '/editor/testgather2')
                return response
            else:
                print(file_form.errors)
            return self.get(request, file_form)

    def update(self, request):
        print 'in update'
        fileID = request.GET.get("id", None)
        newName = request.GET.get("newName", None)
        
        # get current file
        file = File.objects.filter(id=fileID).first()
        oldName = file.name 
        path = file.path

        error = 'false' 
        try:
            file.name = newName
            file.save()
            #file.full_clean()
            renameFile(path, oldName, newName)
            #deleteFile(path, oldName)
        except Exception as e:
            print e
            error = 'true'    
            file.name = oldName
            file.save

        data = {'error': error,
                'fID': fileID,
                'newName': newName,
                'oldName': oldName}
        return HttpResponse(json.dumps(data), content_type="application/json") 

    def delete(self, request):
        print('about to delete file')
        fileID = request.POST['fileID']
        delFile = File.objects.get(id=fileID)

        path = getSavedPath() + str(request.user.id) + '/' + str(request.session['projID'])
        # delete file from folder
        deleteFile(path, delFile.name)

        # delete file from database
        print('about to delete file from database')
        delFile.delete()

        # set new current file
        newFiles = File.objects.filter(projectID=request.session["projID"]);
        if (newFiles):
            request.session["currFileID"] = newFiles.first().id
        else:
            request.session["currFileID"] = 0
            path = getSavedPath()  + str(request.session["projID"])
            changeMultDir(path)
            print os.getcwd()

        return HttpResponseRedirect('/editor/IDE/') 

class FileView(View):

    # switch files
    def get(self, request):
        context = RequestContext(request)

        fileID = request.GET.get("fileID", False)
        newFile = File.objects.get(id=fileID)
        request.session["currFileID"] = newFile.id
        currFileName = getCurrFileName(request)
        currFileText = readCurrFile(request)
        currFiles = File.objects.filter(id=request.session["currFileID"])
        currFile = currFiles.first()
        #currFile = serializers.serialize('json', [currFile,])
        projectName = Project.objects.get(id=request.session["projID"]).name

        return HttpResponse(json.dumps({
            'projectName': projectName,
            #'lastSaved': currFile.lastSaved.strftime("%b %d, %Y, %I:%M %p"),
            'currFileName': currFileName,
            'currFileText': currFileText
            }), content_type="application/json")

    # save updated text to file
    def post(self, request):
        context = RequestContext(request)

        # change var name?
        prevFileText = request.POST.get("currFileText", "")
        if (request.session["currFileID"] is not 0):
            # why do i need .first here?
            currFile = File.objects.filter(id=request.session["currFileID"]).first()
            if (currFile is not None):
                print 'in if'
                time = datetime.datetime.now()
                currFile.lastSaved = time
                currFile.save()
            saveFile(currFile.path, currFile.name, prevFileText)

        return HttpResponse(json.dumps({
            "result": "success",
            "lastSaved": currFile.lastSaved.strftime("%b %d, %Y, %I:%M %p")
            }), content_type="application/json")

class IDEView(View):

    def get(self, request):
        context = RequestContext(request)
        print 'in ide'
        projID = request.GET.get('sendProjID', False)

        if (projID):
            print('getting IDE from projects')

            # Set request sessions for project ID and project Name
            request.session["projID"] = projID
            project = Project.objects.get(id=projID)
            projName = project.name
            request.session["projName"] = projName

            # set current file (what IDE will have open) to first file in the project
            currFile = File.objects.filter(projectID=projID).first()
            if (currFile is not None):
                request.session["currFileID"] = currFile.id
            else:
                file = createEmptyFile(project)
                request.session["currFileID"] = file.id

        # TODO: check if this properly handles errors
        else:
            print 'else'
            projID = request.session["projID"]
            projName = request.session["projName"]

        print(request.session["projID"])

        # get files for current project from database
        files = File.objects.filter(projectID=projID)
        # get CurrFile info from database
        currFiles = File.objects.filter(id=request.session["currFileID"])
        currFile = currFiles.first()
        currFileName = getCurrFileName(request)
        currFileText = readCurrFile(request)

        return render(request, 'onlineEditor/IDE.html', {
            'projeID': projID,
            'projName': projName,
            'files': files,
            'currFile': currFile,
            #'lastSaved': currFile.lastSaved.strftime("%b %d, %Y, %I:%M %p"),
            'currFileName': currFileName,
            'currFileText': currFileText
        })

class TermCmdView(View):

    def get(self, request):
        print 'in term cmd'
        context = RequestContext(request)

        # receive unsanitized, unvalidated command
        unvalidatedCmd = request.GET.get("cmd", "")
        print unvalidatedCmd

        # runs command through regex expressions
        cmd = validateCmd(unvalidatedCmd)

        # Check if command enetered was invalid 
        if (cmd is None):
            return HttpResponse(json.dumps({
                'output': "Command entered is not allowed",
                'error': True
                }), content_type="application/json")

        # Check if user entered the help command
        elif (cmd == "-h" or cmd == "-H"):
            docs = helpDocs()
            return HttpResponse(json.dumps({
                'output': docs,
                'error': False
                }), content_type="application/json")

        # Command entered was valid
        else:
            output = ""
            try:
                cmdArr = cmd.split(" ")
                '''
                exec_cmd = subprocess.check_output(cmdArr, stderr=subprocess.STDOUT)
                error = False;

                print 'out'
                outputLines = exec_cmd

                print 'decode'
                for s in outputLines:
                    # translates encoded string to readable output
                    output += s.decode()
                '''
                
                exec_cmd = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                err = exec_cmd.stderr.readlines()

                # Check for error in std err
                error = False
                if (len(err) > 0):
                    print 'err'
                    print(err)
                    outputLines = err
                    error = True

                else:
                    print 'out'
                    outputLines = exec_cmd.stdout.readlines()

                print 'decode'
                for s in outputLines:
                    # translates encoded string to readable output
                    output += s.decode('utf-8', 'ignore'    )
                

            except Exception as e:
                print 'except'
                print(e)
                output = e

            #print 'output' + str(output)
            return HttpResponse(json.dumps({
                'output': output,
                'error': error,
                }), content_type="application/json")

# Send the gather request with the template and url
class TestGatherView(View):
    def get(self, request):
        context = RequestContext(request)
        print 'testing gather'
        projects = Project.objects.filter();
        projects = [p.toJSON() for p in projects]
        print projects
        response = render_to_response('onlineEditor/gather.html', {}, context)
        response['BLOX-url'] = '/editor/testgather2'

        print 'finishing gather'
        return response

# returns data for gather
class TestGather2View(View):

    def get(self, request):
        projects = Project.objects.filter();
        if projects is not None:
            projects = [p.toJSON() for p in projects]
            print projects
        else:
            projects = 'projects'
        data = json.dumps({'response': 'Success',
                           'projects': projects})
        return JsonResponse({'response': 'Success',
                           'projects': projects})

class SuggestionsView(View):

    def post(self, request):
        message = request.POST.get("body", "")
        if message:
            try:
                send_mail("Blox Editor Suggestions", message, \
                EMAIL, [EMAIL], fail_silently=False)
            except BadHeaderError:
                return HttpResponse('Invalid header found')
            return HttpResponse(json.dumps({'success': 'success'}), content_type="application/json")
        else:
            return HttpResponse("Make sure fields are entered and valid")

def createEmptyFile(project, request):
    print 'create empty main.c file'
    # add empty file to new project
    filename = "main.c"
    file = File(projectID=project, name=filename)
    # Assuming file is valid
    path = getSavedPath() +str(request.user.id) + '/' +  str(project.id)
    file.path = path
    file.projectID = project
    time = datetime.datetime.now()
    file.lastSaved = time
    file.save()

    makeFile(file.path, file.name)
    return file

            
# TODO: initialize in variable so it is not global?
cmdPattern = []
cmdPattern.append("ls|man .+|-h|-H|dir")  # file and help commands 
cmdPattern.append("gcc|\./.+|make") # c commands
cmdPattern.append("pwd")
cmdPattern.append("cat.*")
cmdPattern.append("gdb.*")

def validateCmd(cmd):
    for i in range(0,len(cmdPattern)):
        match = re.match(cmdPattern[i], cmd)
        if (match is not None):
            return match.string
    return None

def helpDocs():
    docs =  "Commands currently allowed are: \n\n" + \
            "man {command}\n" + \
            "\tProvides further documentation about bash commands\n" +\
            "ls\n" + \
            "\tSee man ls to learn more\n\n" + \
            "dir\n" + \
            "\tSimilar to ls, see man dir to learn more\n\n" + \
            "gcc {c filename.} -o {executable file name}\n" + \
            "\tCompiles your program and creates an " + \
            "executable file if there were no errors.\n\n" + \
            "./{executable file name}\n" + \
            "\tRuns the compiled program" 
    return docs
