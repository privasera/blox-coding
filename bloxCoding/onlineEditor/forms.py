from django import forms
from django.contrib.auth.models import User
from onlineEditor.models import UserProfile, Project, File

class UserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'autofocus': 'autofocus'
        }))
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class ProjectForm(forms.ModelForm):
    name = forms.CharField(max_length=32, widget=forms.TimeInput(attrs={
        'autofocus': 'autofocus',
        'placeholder': 'Enter Project Name'
        }))

    class Meta:
        model = Project
        fields = ('name',)

    def __init__(self, userID=None, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        if (userID is not None):
            self.userID = userID

    def clean(self):
        cleaned_data = super(ProjectForm, self).clean()
        projName = cleaned_data.get('name')
        if (Project.objects.filter(userID=self.userID).filter(name=projName).first()): 
            raise forms.ValidationError('A project with name ' + projName + ' already exists', code='invalid')
        return self.cleaned_data

class FileForm(forms.ModelForm):
    name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={
        'autofocus': 'autofocus', 
        'placeholder': 'Enter file name'}))
    
    class Meta:
        model = File
        fields = ('name',)

    def __init__(self, projectID=None, *args, **kwargs):
        super(FileForm, self).__init__(*args, **kwargs)
        if (projectID is not None):
            self.projectID = projectID

    def clean(self):
        cleaned_data = super(FileForm, self).clean()
        fileName = cleaned_data.get('name')
        if (File.objects.filter(projectID=self.projectID).filter(name=fileName).first()):
            raise forms.ValidationError('A file with name ' + fileName + ' already exists')
        return self.cleaned_data
