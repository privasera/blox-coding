from onlineEditor.models import File
import os, shutil

#SAVED_PATH = "/home/ubuntu/bubble/"
SAVED_PATH = os.getcwd() + '/onlineEditor/codeFiles/'
print 'sp' + SAVED_PATH
'''
for f in TEMP_PATH:
    SAVED_PATH += str(f) + "/"
'''

print('path: '+SAVED_PATH)

def getSavedPath():
    return SAVED_PATH

def goToSavedPath():
    if (not (os.getcwd() is SAVED_PATH)):
        changeDir(SAVED_PATH)

def changeDir(path):
    try:
        os.chdir(path)
        #print(os.getcwd())
    except Exception as e:
        print(e)
        print(os.listdir('.'))

def changeMultDir(path):
    goToSavedPath()
    dirList = path.split('/')
    print('path: '+str(dirList))
    for x in range(0, len(dirList)):
        changeDir(dirList[x])
    #for s in dirList:
    #    changeDir(s) 
def addCodeFilesDir():
    print(getSavedPath())
    goToSavedPath()
    print ('current working dir: ' + os.getcwd())
    try:
        os.mkdir('codeFiles')
        changeDir('codeFiles')
        print('created codeFiles dir')
    except OSError as e:
        print('codeFiles directory already exists')

# precondition: current dir is at codeFiles
# if directory exists, method will recursively delete directory
def makeUserDir(idfolder):
    try:
        os.mkdir(idfolder)
        changeDir(idfolder)
    except OSError as e:
        shutil.rmtree(idfolder)
        makeUserDir(idfolder)

def addProjDir(projectID):
    try:
        os.mkdir(projectID)
        print('project #: ' + projectID)
        print(os.listdir('.'))
        print(os.getcwd())
    except OSError as e:
        shutil.rmtree(projectID)
        addProjDir(projectID)

def doesFileExist(path, name):
    pass

def renameFile(path, oldName, newName):
    changeMultDir(path)
    shutil.move(oldName, path + '/' +  newName)

# not needed
def copyFile(path, oldName, newName):
    changeMultDir(path)
    shutil.copyfile(oldName, path + '/' +  newName)

def makeFile(path, name):
    print("Making file")
    print(path)
    print(name)
    changeMultDir(path)
    print("Changed Directory")
    f = open(name, 'w+')
    print("Wrote")
    f.close()

def saveFile(path, name, text):
    changeMultDir(path)
    f = open(name, 'w+')
    f.seek(0)
    f.write(text)
    f.truncate()
    f.close()

def deleteFile(path, name):
    changeMultDir(path)
    if (os.path.isfile(name)):
        print('folder exists')
        os.remove(name)

def readCurrFile(request):
    currFiles = File.objects.filter(id=request.session["currFileID"])
    currFile = currFiles.first()
    currFileText = ""
    if (currFile is not None):
        print('changing directory')
        changeMultDir(currFile.path)
        print('changed directory')
        with open(currFile.name, 'r+') as myfile:
            fileText = myfile.readlines()
        for s in fileText:
            currFileText += s;
    return currFileText

def getCurrFileName(request):
    currFiles = File.objects.filter(id=request.session["currFileID"])
    currFile = currFiles.first()
    currFileName = "No Files"
    if (currFile is not None):
        currFileName = currFile.name
    return currFileName
