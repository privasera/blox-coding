from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from onlineEditor.forms import UserForm, ProjectForm, FileForm
from onlineEditor.models import UserProfile, Project, File
from onlineEditor.filemanip import *

import os, subprocess, shutil

#Login Views
def register(request):
    context = RequestContext(request)
    #print(SAVED_PATH)
    #print(os.listdir())
    registered = False
    if (request.method == 'POST'):
        user_form = UserForm(data=request.POST)
        profile = UserProfile()

        if (user_form.is_valid()): 
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            #profile
            profile.user = user
            profile.save()
            print('user '+ str(profile.user))
            print("registration success")
            registered = True
            gotoSavedPath()
            changeDir('onlineEditor')
            changeDir('codeFiles')
            makeUserDir(str(user.id))
            changeDir(SAVED_PATH)
            return HttpResponseRedirect('/editor/login/')
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
        #profile_form = UserProfileForm()

    return render_to_response('onlineEditor/register.html', {
        'user_form': user_form, 
        #'user_profile_form': profile_form,
        'registered': registered
        }, context)

def user_login(request):
    context = RequestContext(request)
    if (request.user.is_authenticated()):
        return HttpResponseRedirect('/editor/')
    if (request.method == 'POST'):
        username = request.POST['username']
        password = request.POST['password']

        userProf = authenticate(username = username, password=password)
        if (userProf):
            login(request, userProf)
            print('user ' + userProf.get_username())
            print("Login success")
            gotoSavedPath()
            changeDir('onlineEditor')
            changeDir('codeFiles')
            idfolder = str(userProf.id)
            try:
                os.chdir(idfolder)
                print(os.getcwd())
            except:
                makeUserDir(idfolder)
            return HttpResponseRedirect('/editor/projects/')
        else:            
            print("Invaled login details")
            return render_to_response('onlineEditor/login.html', {'error': "Invalid login"}, context)
    else:
        return render_to_response('onlineEditor/login.html', {}, context)

@login_required
def user_logout(request):
    logout(request)
    gotoSavedPath()
    return HttpResponseRedirect('/editor/')


# Create your views here
def index(request):
    randomVar = 'Can i see this on client side?'
    return render(request, 'onlineEditor/index.html', )

@login_required
def projects(request):
    #print('id: '+str(request.user.id))
    try:
        print('user: '+str(request.user))
        print('userID: '+str(request.user.id))
        print(Project.objects.filter(userID=request.user.id-1))
        #projects = Project.objects.all()
        projects = Project.objects.filter(userID=request.user.id-1)
        return render(request, 'onlineEditor/projects.html', { 'projects': projects})
    except:
        return render(request, 'onlineEditor/projects.html', )

@login_required
def IDE(request):
    context = RequestContext(request)
    if (request.method == 'POST'):
        #projID = request.POST['sendProjID']
        projID = request.POST.get('sendProjID', False)
        request.session["projID"] = projID
        projName = Project.objects.get(id=projID).name
        request.session["projName"] = projName
        currFile = File.objects.filter(projectID=projID).first()
        if (currFile is not None):
            request.session["currFileID"] = currFile.id
        else: 
            request.session["currFileID"] = 0
        #print('projId: ' + str(projID))
    projID = request.session["projID"]
    projName = request.session["projName"]
    files = File.objects.filter(projectID=projID)
    #print(files)
    # load first file
    print(request.session["currFileID"])
    currFiles = File.objects.filter(id=request.session["currFileID"])
    currFile = currFiles.first()
    #print(currFile)
    currFileName = getCurrFileName(request)
    currFileText = readCurrFile(request)
    return render_to_response('onlineEditor/IDE.html', { 
        'projeID': projID,
        'projName': projName,
        'files': files,
        'currFileName': currFileName,
        'currFileText': currFileText
    }, context)
    #return render_to_response('onlineEditor/IDE.html', {}, context)

@login_required
def add_project(request):
    context = RequestContext(request)
    if (request.method == 'POST'):
        name = request.POST['name']
        project_form = ProjectForm(data=request.POST)
        if (project_form.is_valid()):
            print('user '+str(request.user.id))
            project = project_form.save(commit=False)
            project.name = name
            project.userID = UserProfile.objects.get(user=request.user)
            project.save()

            # creating the directory for the project
            print('desired path: '+ 'SAVED_PATH' + 
                '/onlineEditor/codeFiles/'+str(request.user.id))
            '''
            if (not (os.getcwd() is 'SAVED_PATH' + 
                '/onlineEditor/codeFiles/'+str(request.user.id))):
            '''
            gotoSavedPath()
            changeDir('onlineEditor')
            changeDir('codeFiles')
            changeDir(str(request.user.id))
            addProjDir(str(project.id))

            return HttpResponseRedirect('/editor/projects/')
        else:
            print(project_form.errors)
    else:
        project_form = ProjectForm()
    return render_to_response('onlineEditor/addProject.html', {
        'project_form': project_form 
        }, context)

@login_required
def delete_project(request):
    context = RequestContext(request)
    if (request.method == 'POST'):
        projID = request.POST['delProjID']
        print('delete ' + projID)
        print('proj: '+str(Project.objects.get(id=projID)))
        Project.objects.get(id=projID).delete()
        '''
        if (not (os.getcwd() is 'SAVED_PATH' + 
            '/onlineEditor/codeFiles/'+str(request.user.id))):
            changeDir(SAVED_PATH)
        '''
        gotoSavedPath()
        changeDir('onlineEditor')
        changeDir('codeFiles')
        changeDir(str(request.user.id))
        print(os.listdir())
        shutil.rmtree(projID)
        return HttpResponseRedirect('/editor/projects/')

@login_required
def add_file(request):
    context = RequestContext(request)
    if (request.method == 'POST' and 'submit' in request.POST):
        #print('uh oh')
        #name = request.POST['name']
        name = request.POST.get('name', False)
        print('filename ' + name)
        file_form = FileForm(data=request.POST)
        projID = request.session["projID"]
        print('pID: ' + projID)
        if (file_form.is_valid()): 
            file = file_form.save(commit=False)           
            path = 'onlineEditor/codeFiles/' + \
                str(request.user.id) + '/' + str(projID)
            file.name = name
            file.path = path
            file.projectID = Project.objects.get(id=projID)
            file.save()

            # make physical file in project dir
            changeMultDir(file.path)
            f = open(file.name, 'w+')
            f.close()
            print('id: ' + str(file.id))
            request.session["currFileID"] = file.id

            return HttpResponseRedirect('/editor/IDE/')
        else:
            print(file_form.errors)
    else:
        file_form = FileForm()
    return render_to_response('onlineEditor/addFile.html', {
        'file_form': file_form,
        }, context)

@login_required
def delete_file(request):
    context = RequestContext(request)
    if (request.method == 'POST'):
        fileID = request.POST['fileID']
        print('fID: '+fileID)
        delFile = File.objects.get(id=fileID)
        print('hello')
        # delete physical file
        gotoSavedPath()
        changeDir('onlineEditor')
        changeDir('codeFiles')
        changeDir(str(request.user.id))
        changeDir(str(request.session['projID']))
        print('currpath: '+ str(os.listdir()))
        os.remove(delFile.name)

        # delete file from database
        delFile.delete()
        newFiles = File.objects.filter(projectID=request.session["projID"]);
        if (newFiles):
            request.session["currFileID"] = newFiles.first().id
        else:
            request.session["currFileID"] = 0
        print('files: ' + str(newFiles))
    return HttpResponseRedirect('/editor/IDE/')

@login_required
def switch_files(request):
    context = RequestContext(request)
    print('switching')
    prevFileText = request.POST.get("currFileText", "")
    print('saved text: '+ prevFileText)
    fileID = request.POST.get("fileID", False)
    print('id: '+str(fileID))
    print(request.session["currFileID"])
    if (request.session["currFileID"] is not 0):
        currFile = File.objects.filter(id=request.session["currFileID"]).first()
        print('file: '+str(currFile.name))
        if (currFile is not None):
            # save text to file
            #file = File.objects.get(id=fileID)
            changeMultDir(currFile.path)
            f = open(currFile.name, 'w+')
            f.seek(0)
            f.write(prevFileText)
            f.truncate()
            f.close()
    newFile = File.objects.get(id=fileID)
    print('new: '+ str(newFile.name))
    request.session["currFileID"] = newFile.id
    currFileName = getCurrFileName(request)
    currFileText =  readCurrFile(request)
    response_data = {}
    response_data['currFileName'] = currFileName
    response_data['currFileText'] = currFileText
    print(response_data)
    return HttpResponse(json.dumps(response_data), content_type="application/json")
    print("this shouldn't print ")

@login_required
def term_cmd(request):
    context = RequestContext(request)
    cmd = request.POST.get("cmd", "")
    print(cmd);
    output = ""
    try:
        cmdArr = cmd.split(" ")
        exec_cmd = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        err = exec_cmd.stderr.readlines()
        error = False
        if (len(err) > 0):
            print('err '+ str(err))
            outputLines = err
            error = True
        else:
            outputLines = exec_cmd.stdout.readlines()
        output
        for s in outputLines:
            output += s.decode() + " "
        print('out: '+output)
        
    except Exception as e:
        print(e)
        output = e
        print('err out: ' + output)
    return HttpResponse(json.dumps({ 'output': output, 'error': error}), content_type="application/json")

def readCurrFile(request):
    currFiles = File.objects.filter(id=request.session["currFileID"])
    currFile = currFiles.first()
    currFileText = ""
    if (currFile is not None):
        #print('name: '+currFile.name)
        changeMultDir(currFile.path)
        with open(currFile.name, 'r+') as myfile:
            fileText = myfile.readlines()
        for s in fileText:
            currFileText += s;
    print(currFileText)
    return currFileText

def getCurrFileName(request):
    currFiles = File.objects.filter(id=request.session["currFileID"])
    currFile = currFiles.first()
    currFileName = "No Files"
    if (currFile is not None):
        currFileName = currFile.name
    return currFileName