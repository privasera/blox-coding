var post = function(url, data, success) {
    $.ajax({
        url: url,
        type: "POST",
        datatype: "json",
        data: data,
        success: success,
        error: function(jqXHR, text, error) {
            alert(text + ": " + error);
        }
    });
    return false;
};

var post = function(url, data, success, complete) {
    $.ajax({
        url: url,
        type: "POST",
        datatype: "json",
        data: data,
        success: success,
        complete: complete
    });
    return false;
};

var get = function(url, data, success) {
    $.ajax({
        url: url,
        type: "GET",
        datatype: "json",
        data: data,
        success: success
    });
    return false;
}

var get = function(url, data, success, complete) {
    $.ajax({
        url: url,
        type: "GET",
        datatype: "json",
        data: data,
        success: success,
        complete: complete
    });
    return false;
}