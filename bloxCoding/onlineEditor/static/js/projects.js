var delProjSuccess = function(pID, json) {
    return true;
}

var delProjComplete = function() {
    window.location = "/editor/projects/";
}

function delProj(event, pID) {
    event.preventDefault();
    post('/editor/deleteproject/', { 
        'projID': pID 
    }, delProjSuccess(), delProjComplete);
    
    return false;
}