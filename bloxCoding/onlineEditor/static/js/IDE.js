var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/c_cpp");
editor.getSession().setUseWrapMode(true);
editor.setShowPrintMargin(true);
editor.resize();
focusEditor();

function focusEditor() {
    editor.focus();
    var len = editor.getSession().getLength();
    editor.gotoLine(len);
}

var saveFilePostSuccess = function(json) { 
    return true;
}

var saveFileGetSuccess = function(json) {            
    $('#editorDiv .title').html("<h4>" + json['projectName'] + " - " + json['currFileName'] + "</h4>");
    editor.getSession().setValue(json['currFileText']);
    editor.resize();
    focusEditor();
};

var saveFile = function() {
    post('/editor/savefile/', {
        'currFileText': editor.getValue()
    }, saveFilePostSuccess);
}
    
var switchFile = function(event, fID) {
    event.preventDefault();
    saveFile();
    get('/editor/getfile/', {
        'fileID': fID
    }, saveFileGetSuccess);
    return false;
    
}

var termCmdSuccess = function(json) {
    $('#termOut').append("<span> " + $('#console').val() + "</span><br />");
    if (!(json['output'] === ""))
    {
        if (json['error']) {
            $('#termOut').append("<span class='text-danger'>" + json['output'] + "</span><br />");
        }
        else {
            $('#termOut').append("<span>" + json['output'] + "</span><br />");
        }                
    }
    $('#termOut').append(">");
    $('#console').val("");
    return true;
}

var termCmdComplete = function() {
    var elem = document.getElementById('terminal');
    elem.scrollTop = elem.scrollHeight;
    return true;
}

$('#termCmd').submit(function(e) {
    e.preventDefault();
    saveFile();
    get('/editor/termCmd/', {
        'cmd': $('#console').val()
    }, termCmdSuccess, termCmdComplete);

});