from django.conf.urls import url
#from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^projects/$', views.ProjectDetailView.as_view(), name='projects'),
    url(r'^addproject/$', views.ProjectFormView.as_view(), name='addproject'),
    url(r'^deleteproject/$', views.ProjectFormView.as_view(), name='deleteproject'),
    url(r'^IDE/$', views.IDEView.as_view(), name='IDE'),
    url(r'^addfile/$', views.FileFormView.as_view(), name='addfile'),
    url(r'^deleteFile/$', views.FileFormView.as_view(), name='deletefile'),
    url(r'^savefile/$', views.FileView.as_view(), name='savefile'),
    url(r'^getfile/$', views.FileView.as_view(), name='getfile'),
    url(r'^termCmd/$', views.TermCmdView.as_view(), name='termcmd')
]
