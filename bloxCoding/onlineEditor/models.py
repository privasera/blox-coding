from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres import fields

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    def __str__(self):
        return self.user.get_username()

class Project(models.Model):
    userID = models.ForeignKey(UserProfile, null=True)
    name = models.CharField(max_length = 32, default="")
    def __str__(self):
        return self.name

class File(models.Model):
    projectID = models.ForeignKey(Project, null=True, related_name='project')
    name = models.CharField(max_length = 32, default="")
    path = models.CharField(max_length = 128, blank=True, default="")
    def __str__(self):
        return self.name